/** flattenArray recursively flattens array
 * @param items Array to be flattened
 *
 * Input: [[1, 2, 3], [101, 2, 1, 10], [2, 1]]
 *
 * Output = [1, 2, 3, 101, 2, 1, 10, 2, 1]
 */

export const flattenArray = (items: Array<any>): Array<any> => [].concat.apply([], items)
export default flattenArray
