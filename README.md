# flatten-array-ts

Gitlab: https://gitlab.com/reedrichards-javascript/typescript/flatten-array

Docs: https://reedrichards-javascript.gitlab.io/typescript/flatten-array/index.html

## Install

```bash
npm install flatten-array-ts
```

## Usage

```javascript
import flattenArray from 'flattenArray'

flattenArray([[1, 2, 3], [101, 2, 1, 10], [2, 1]])
//  Output: [1, 2, 3, 101, 2, 1, 10, 2, 1]
```

## Development

### NPM scripts

- `npm t`: Run test suite
- `npm start`: Run `npm run build` in watch mode
- `npm run test:watch`: Run test suite in [interactive watch mode](http://facebook.github.io/jest/docs/cli.html#watch)
- `npm run test:prod`: Run linting and generate coverage
- `npm run build`: Generate bundles and typings, create docs
- `npm run lint`: Lints code
