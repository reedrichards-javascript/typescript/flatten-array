import flattenArray from '../src/flattenArray'
/**
 * Tests for flattenArray
 */
const input = [[1, 2, 3], [101, 2, 1, 10], [2, 1]]
const output = [1, 2, 3, 101, 2, 1, 10, 2, 1]

describe('flattenArray', () => {
  it('flattenArray flattens array', () => {
    expect(flattenArray(input)).toEqual(output)
  })
})
